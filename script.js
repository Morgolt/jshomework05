/* 1. Опишіть своїми словами, що таке метод об'єкту.

Об'єкт - це набір властивостей, і кожна властивість складається з імені та значення, асоційованого з цим ім'ям.

Значенням властивості може бути функція, яку можна назвати методом об'єкта.

2. Який тип даних може мати значення властивості об'єкта?

Може мати будь-який тип даних.

3. Об'єкт це посилальний тип даних. Що означає це поняття?

Оскільки об'єкти можуть містити дуже великі обсяги різнорідних даних, змінна, що містить посилальний тип данних, фактично його значення не містить.

Вона містить посилання на місце у пам'яті, де розміщуються реальні дані.

*/

function createNewUser () {
  const firstName = prompt("Введіть Ваше ім'я");
  const lastName = prompt('Введіть Ваше прізвище');
  const newUser = {};
  Object.defineProperties(newUser, {
    firstName: {
      value: firstName
    },
    lastName: {
      value: lastName
    },
    getLogin: {
      value: function () {
        const userNew = this.firstName[0] + this.lastName;
        return userNew.toLowerCase();
      }
    }
  });
  return newUser;
};
const userDB = createNewUser();
console.log(userDB.getLogin());
